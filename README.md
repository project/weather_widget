-- SUMMARY --
Weather Widget provides a weather widget displayed as block which can be added from Blocks settings. 


-- REQUIREMENTS --

- openweathermap API key, https://openweathermap.org/api
- google API key, https://developers.google.com/maps/documentation/geocoding/get-api-key


-- INSTALLATION --

* Standard module installation applies. See
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


-- CONFIGURATION --

 - You can reach Weather_widget settings by going to this path: 
  Administration » Configuration » User interface » Weather widget Settings
 - you will have to fill the openweathermap API key and Google API key from Weather widget Settings.
 - you can change the Weather widget Units format by selecting the options provided.





-- Maintainers --

- Ahmed Alhaddad (ahmedx) https://www.drupal.org/u/ahmedx
- Mohammed Bamlhes (mbamlhes) https://www.drupal.org/u/bamlhes