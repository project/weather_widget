<?php $item = reset($items);
$path_weather = drupal_get_path('module', 'weather_widget');
$background_weather = file_create_url($path_weather . '/assets/images/weather-widget.jpg');
?>
<div class="widget">
	<div class="banner">
		<div class="bg-image" style="background-image: url(<?php print $background_weather; ?>)"></div>
		<div class="baner-top inline-items">

			<div class="w-right">
				<img alt="few clouds" src="https://openweathermap.org/img/w/<?php print $item->weather[0]->icon; ?>.png">

			</div>
			<div class="w-left">
				<div class="max-min-temperature">

					<span class="number"><?php print (int)$item->temp->min;?>°</span>
					<span class="number"><?php print (int)$item->temp->max;?>°</span>
				</div>
				<div class="number temperature-sensor mr-3"><?php print (int)$item->temp->day;?>°</div>
			</div>

		</div>
		<div class="weather-timeline">
		<?php  foreach (array_slice($items,1) as $key => $value) { ?>
			<div class="weather-timeline-item">
				<div class="weather-item-day"> <?php print format_date($value->dt,'custom',"D"); ?> </div> 
				<div class="weather-item-icon"> <img alt="few clouds" src="https://openweathermap.org/img/w/<?php print $value->weather[0]->icon;?>.png"> </div> 
				<div class="weather-item-temp number"> <?php print (int)$value->temp->day;?>° </div> 
			</div>
		<?php  } ?>
		</div>
		<div class="banermeta">
		<?php

        print '<p class="bold">'.$info['city'].' , '.$info['country'].' </p>';

    	?>
			
		</div>
	</div>											
</div>