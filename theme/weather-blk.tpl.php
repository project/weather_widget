<?php
$path_weather = drupal_get_path('module', 'weather_widget');
$background_weather = file_create_url($path_weather . '/assets/images/weather-widget.jpg');
?>
<div class="weather-widget">
	<div class="widget">
		<div class="banner">
			<div class="bg-image" style="background-image: url(<?php print $background_weather; ?>)"></div>
			<div class="banermeta" style="text-align: center;">
				<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
				<p class="bold" style="text-transform: none;"><?php print t('Please allow the browser to know your location to activate the weather status'); ?></p>
			</div>
		</div>											
	</div>
</div>



	<script type="text/javascript">

			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function (position) {
					var lat = position.coords.latitude;
					var lng = position.coords.longitude;

						jQuery.ajax({
							url: '<?php print url('weather-blk-ajax'); ?>',
							type: 'POST',
							data:{
								lat: lat,
								lng: lng
							},
							success: function (data) {
							jQuery('.weather-widget').empty();
							jQuery('.weather-widget').append(data);
					},
					cache: false
				});
					
				});
			}


	</script>